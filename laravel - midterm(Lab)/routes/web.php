<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/activity1', function () {
    return view('activity1');
});

Route::get('/login', function () {
    return view('login');
});

Route::get('/home', function () {
    return view('home');
});

Route::get('/register', function () {
    return view('register');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/student', function () {
    return view('student');
});

Route::get('student/details', function () {
    $url="Name: Norene Ann B. Rabara" . "<br>" . "Section: 3C";
    return $url;
})->name('student.details');


Route::get('/item/{itemno}/{name}/{price}', function ($itemno, $name, $price) {
    return "Item No.: " . $itemno . "<br>" . "Name: " . $name . "<br>" . "Price: " . $price. "<br><br>" . "Norene Ann B.Rabara"."<br>"."3C";
});

Route::get('/customer/{id}/{name}/{address}/{age?}', function ($id, $name, $address, $age=null) {
    return "ID: " . $id . "<br>" . "Name: " . $name . "<br>" . "Address: " . $address . "<br>" . "Age: " . $age. "<br><br>" . "Norene Ann B.Rabara"."<br>"."3C";
});

Route::get('/order/{customerID}/{name}/{orderNo}/{date}', function ($customerID, $name, $orderNo, $date) {
    return "Customer ID: " . $customerID . "<br>" . "Name: " . $name . "<br>" . "Order No: " . $orderNo . "<br>" . "Date: " . $date. "<br><br>" . "Norene Ann B.Rabara"."<br>"."3C";
});

Route::get('/orderdetails/{transNo}/{orderNo}/{itemID}/{name}/{price}/{qty}/{receiptNum?}', function ($transNo, $orderNo, $itemID, $name, $price, $qty, $receiptNum=null) {
    return "TransNo: " . $transNo . "<br>" . "Order No: " . $orderNo . "<br>" . "Item ID: " . $itemID . "<br>" . "Name: " . $name . "<br>" . "Price: " . $price . "<br>" . "Quantity: " . $qty . "<br>" . "receiptNum: " . $receiptNum . "<br><br>" . "Norene Ann B.Rabara"."<br>"."3C";
});

Route::redirect('/here', '/there', 404);
