<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel - Login</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

        <style>
            .hey{
                position: relative;
                left: 17px;
                top: 18px;
                z-index:2;
            }
            .float{
                position: relative;
                left: 100px;
                bottom: 68px;
                z-index:2;
            }
            .container{
                position: relative;
                left: 20px;
                top: 170px;
                right: 150px;
                bottom: 50px;
            }
            body{
                background-image: url('images/loginbg.jpg');
            }
        </style>
</head>
<body>
<nav class="navbar navbar-expand-lg bg-light navbar-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">
      <img src="images/logoo.jpg" style="width:60px; height: 40px; margin-left: 100px;" class="rounded-pill"> 
    </a>
<div class="collapse navbar-collapse justify-content-between">
<ul class="nav justify-content-end" style="margin-left:600px;">
  <li class="nav-item">
    <a class="nav-link active" aria-current="page" href="home">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="about">About</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="contact">Contact Us</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="login">Log In</a>
    <li class="nav-item">
    <a class="nav-link" href="register">Registration</a>
  </li>
  </li>
</ul>
        </div>
        </nav>
   <form action="home" method="get"> 
    <div class="container">
        <div class="row">
            <div class="col mx-auto mt-2">
                <div class="card">
                    <h5 class="hey">Log in Form</h5>
                    <div class="card-body">
                    <div class="float-end">
                    <img  src="images/profile.png" style="height:200px; width:300px; margin-right: 200px; margin-bottom: 5px;">
        </div>
                        <h6 class="card-subtitle mb-2 mt-2">Email : </h6>
                        <input type="text" placeholder="Enter your email">
                        <br>
                        <h6 class="card-subtitle mb-2 mt-2">Password : </h6>
                        <input type="password" class="pass" placeholder="Enter your password">
                        <br><br>
                        <button type="submit" class="btn btn-primary">Log in</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
        </form>
</body>
</html>