<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel - Activity 1</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

        <style>
            .float{
                position: relative;
                left: 17px;
                top: 18px;
                z-index:2;
            }
            .town{
                color: red;
                margin-top: 5px;
                margin-bottom: 5px;
            }
            .pass{
                border: 5px solid #FF0000;
            }
            .click{
                border: 2px solid #FF0000;
            }
        </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col mx-auto mt-2">
            <h5 class="float">Personal Information</h5>
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-subtitle mb-2">Firstname : </h6>
                        <input type="text" placeholder="Enter your firstname">
                        <br>
                        <h6 class="card-subtitle mb-2 mt-2">Lastname : </h6>
                        <input type="text" placeholder="Enter your lastname">
                        <br>
                        <h6 class="card-subtitle mb-2 mt-2">Username : </h6>
                        <input type="text" placeholder="Enter your username">
                        <br>
                        <h6 class="card-subtitle mb-2 mt-2">Password : </h6>
                        <input type="password" class="pass" placeholder="Enter your password">

                        <div class="form-floating mt-3">
                            <textarea class="form-control" placeholder="Enter your comment" id="floatingTextarea2" style="height: 150px; width: 550px;">In an interview,  Joey shared his back story of how he had to go through bankruptcy and loss of programmers. His first real business was a pizza delivery. His first IT business was Macintosh.
That is when Joey was known in the field of IT, forming and running successful software companies. He founded several companies during the 90s including Match Data Systems (MDS) USA in 1987, MDS Philippines in 1991, and MDS Australia in 1996. He also sold MDS to Great Plains Software, which was bought by Microsoft in 2001.
Joey has co-founded several other software start-ups such as SPRING.ph. Joey served as the Asia Pacific Regional Director for Microsoft Business Solutions, before he left to form his own Gurango Software.
</textarea>
        </div>
        <h6 class="card-subtitle mb-2 mt-2">Birthdate : <input type="date"></h6>
        <div class="town">
            <label>Town:</label>
        <select>
            <option selected>Select your town</option>
            <option value="Agno">Agno</option>
            <option value="Aguilar">Aguilar</option>
            <option value="Alaminos">Alaminos</option>
            <option value="Alcala">Alcala</option>
            <option value="Anda">Anda</option>
            <option value="Asingan">Asingan</option>
            <option value="Balungao">Balungao</option>
            <option value="Bani">Bani</option>
            <option value="Basista">Basista</option>
            <option value="Bautista">Bautista</option>
            <option value="Bayambang">Bayambang</option>
            <option value="Binalonan">Binalonan</option>
            <option value="Binmaley">Binmaley</option>
            <option value="Bolinao">Bolinao</option>
            <option value="Bugallon">Bugallon</option>
            <option value="Burgos">Burgos</option>
            <option value="Calasiao">Calasiao</option>
            <option value="Dagupan City">Dagupan City</option>
            <option value="Dasol">Dasol</option>
            <option value="Infanta">Infanta</option>
            <option value="Labrador">Labrador</option>
            <option value="Laoac">Laoac</option>
            <option value="Lingayen">Lingayen</option>
            <option value="Mabini">Mabini</option>
            <option value="Malasiqui">Malasiqui</option>
            <option value="Manaoag">Manaoag</option>
            <option value="Mangaldan">Mangaldan</option>
            <option value="Mangatarem">Mangatarem</option>
            <option value="Mapandan">Mapandan</option>
            <option value="Natividad">Natividad</option>
            <option value="Pozorrubio">Pozorrubio</option>
            <option value="Rosales">Rosales</option>
            <option value="San Carlos">San Carlos</option>
            <option value="San Fabian">San Fabian</option>
            <option value="San Jacinto">San Jacinto</option>
            <option value="San Manuel">San Manuel</option>
            <option value="San Nicolas">San Nicolas</option>
            <option value="San Quintin">San Quintin</option>
            <option value="Santa Barbara">Santa Barbara</option>
            <option value="Santa Maria">Santa Maria</option>
            <option value="Santo Tomas">Santo Tomas</option>
            <option value="Sison">Sison</option>
            <option value="Sual">Sual</option>
            <option value="Tayug">Tayug</option>
            <option value="Umingan">Umingan</option>
            <option value="Urbiztondo">Urbiztondo</option>
            <option value="Urdaneta City">Urdaneta City</option>
            <option value="Villasis">Villasis</option>
        </select>
        </div>
        <h6 class="card-subtitle mb-2 mt-2">Browser : <input type="text"></h6>
        <button type="button" class="click" style="height: 45px; width: 80px; background-color:#008000; color: #FFFFFF">Click me</button>
        <input type="submit" value="Reset">
        <input type="submit" value="pindot me">
</body>
</html>