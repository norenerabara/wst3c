<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <title>Homepage</title>
    <style>
        a{
            text-decoration: none;
            color: black;
            font-weight: bold;
        }

        a:hover{
            color:white;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-sm bg-dark">
<div class="container-fluid">
<ul class="nav justify-content-center">
    <li class="nav-item">
    <a href = "{{ route('item') }}">Item</a>
    </li>
    <li class="nav-item">
    <a href = "{{ route('customer') }}">Customer</a>
    </li>
    <li class="nav-item">
    <a href = "{{ route('order') }}">Order</a>
    </li>
    <li class="nav-item">
    <a href = "{{ route('orderDetails')}}">Order Details</a>
    </li>
  </ul>
</div>
</nav>
</body>
</html>