<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel - Registration Form</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

        <style>
            .hey{
                position: relative;
                left: 17px;
                top: 18px;
                z-index:2;
            }
            .container{
                position: relative;
                left: 20px;
                top: 120px;
                right: 150px;
                bottom: 50px;
            }
            .float{
                position: relative;
                left: 60px;
                top: 50%;
                z-index:2;
            }
            body{
                background-image: url('images/reg.jpg');
            }
        </style>
</head>
<body>
<nav class="navbar navbar-expand-lg bg-light navbar-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">
      <img src="images/logoo.jpg" style="width:60px; height: 40px; margin-left: 100px;" class="rounded-pill"> 
    </a>
<div class="collapse navbar-collapse justify-content-between">
<ul class="nav justify-content-end" style="margin-left:600px;">
  <li class="nav-item">
    <a class="nav-link active" aria-current="page" href="home">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="about">About</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="contact">Contact Us</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="login">Log In</a>
    <li class="nav-item">
    <a class="nav-link" href="register">Registration</a>
  </li>
  </li>
</ul>
        </div>
        </nav>
<form action="home" method="get"> 
    <div class="container">
        <div class="row" >
            <div class="col mx-auto mt-2">
                <div class="card"style="padding: 8px;">
                    <h5 class="hey">Registration Form</h5>
                    <div class="card-body">
                    <div class="float-end">
                    <img  src="images/regbook.png" style="height:200px; width:300px; margin-right: 200px; margin-top: 40px">
        </div>
                    <h6 class="card-subtitle mb-2 mt-2">Firstname : </h6>
                        <input type="text" placeholder="Enter your firstname">
                        <br>
                        <h6 class="card-subtitle mb-2 mt-2">Lastname : </h6>
                        <input type="text" placeholder="Enter your lastname">
                        <br>
                        <h6 class="card-subtitle mb-2 mt-2">Email : </h6>
                        <input type="text" placeholder="Enter your email">
                        <br>
                        <h6 class="card-subtitle mb-2 mt-2">Password : </h6>
                        <input type="password" class="pass" placeholder="Enter your password">
                        <br><br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary">Register</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
        </form>
                        
</body>
</html>