<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&family=Spline+Sans+Mono&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&family=Acme&family=Courgette&family=Kdam+Thmor+Pro&family=Pacifico&family=Spline+Sans+Mono&display=swap" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <title>Menu</title>

    <style>
        .body{
            background-color: #000;
        }
        .h5{
            color: #fff;
        }
        .nav-link{
            color: #fff;
        }
        .title{
            color: #fff;
            font-style: bold;
            font-family: 'Spline Sans Mono', monospace;
            font-size: 30px;
            margin-left: 30px;
        }
        .container{
            margin-top: 10px;
            margin-bottom: 150px;
        }
        .btn{
            margin-top: 10px;
            margin-left: 550px;
        }
        .nav-link:hover{
          color: #000;
        }
        .nav a{
          font-family: 'Kdam Thmor Pro', sans-serif;
          color: #fff;
        }
    </style>
    
</head>
<body class="body">

<header class="header">
    <nav class="navbar navbar-expand-lg bg-danger">
      <div class="container-fluid">
          <div class="title"> 
          Cafemoza
    </div>
            <ul class="nav justify-content-end">
                <li class="nav-item">
                    <a class="nav-link" href="adminhome">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="adminmenu">Menu</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="about">About Us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="products">Products</a>
                </li>
            </ul>
        </div>
      </div>
    </nav>
  </header>
  <div class="btn">
  <a href="/about"><button type="submit" class="btn btn-light">Back</button></a></div>
  <form action="/create" method="post">
<input type = "hidden" name = "_token" value = "<?php echo csrf_token();?>">
<div class="container">
<div class="card">
  <div class="card-body">
  <div class="mb-3">
  <label for="exampleFormControlInput1" class="form-label">Name</label>
  <input type="text" class="form-control" id="exampleFormControlInput1" name="name" placeholder="Name">
  <br>
  <div class="mb-3">
  <label for="exampleFormControlInput1" class="form-label">Position</label>
  <input type="text" class="form-control" id="exampleFormControlInput1" name="position" placeholder="Position">
  <br>
  <label for="exampleFormControlInput1" class="form-label">Photo</label>
  <input type="file" class="form-control" id="exampleFormControlInput1" name="photo" placeholder="Photo">
  <br>
  <button type="submit" class="btn btn-dark">Add</button>
</div>
</div>
  </div>
</div>
    </div>
</form>
<footer class="text-center text-dark fixed-bottom" style="background-color: #fff;">
  <!-- Grid container -->

  <!-- Copyright -->
  <div class="text-center p-1">
    © 2022 Copyright
    <p>Designed and Developed by: Norene Ann B. Rabara</p>
  </div>
  <!-- Copyright -->
</footer>
    
</body>

</html>