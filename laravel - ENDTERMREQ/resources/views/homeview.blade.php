<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&family=Acme&family=Pacifico&family=Spline+Sans+Mono&display=swap" rel="stylesheet">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&family=Acme&family=Courgette&family=Kdam+Thmor+Pro&family=Pacifico&family=Spline+Sans+Mono&display=swap" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <title>Home</title>

    <style>
        .body{
            background-color: #000;
        }
        .h5{
            color: #fff;
        }
        .nav-link{
            color: #fff;
        }
        .title{
            color: #fff;
            font-style: bold;
            font-family: 'Spline Sans Mono', monospace;
            font-size: 30px;
            margin-left: 30px;
        }
        .container{
          position: relative;
          text-align: center;
          color: white;
          margin-left: 0px;
          margin-right: 0px;
        }
        .centered {
          position: absolute;
          top: 50%;
          left: 60%;
          color: #ffc107;
          --bs-bg-opacity: 50;
          font-family: 'Acme', sans-serif;
          font-size: 90px;
          transform: translate(-50%, -50%);
        }
        .contaner-fluid{
          margin-bottom: 100px;
        }
        .nav{
          color: #000;
          font-family: 'Kdam Thmor Pro', sans-serif;
          font-style: bold;
          font-size: 15px;
        }
        .current:hover{
          color: #000;
        }
        .nav-linkk{
          color: #000;
          font-style: bold;
          font-size: 15px;
        }
        .nav-link:hover{
          color: #000;
        }
        .nav a{
          font-family: 'Kdam Thmor Pro', sans-serif;
          color: #fff;
        }
    </style>
    
</head>
<body class="body">
<form method="post" action="{{ route('login.check') }}">
<input type = "hidden" name = "_token" value = "<?php echo csrf_token();?>">
<header class="header">
    <nav class="navbar navbar-expand-lg bg-danger">
      <div class="container-fluid">
          <div class="title">
          &nbsp;&nbsp;&nbsp;&nbsp;<img src="images/logo.jpg" style="width:55px; height: 53px;" class="rounded-pill"> 
          Cafemoza
    </div>
            <ul class="nav justify-content-end">
                <li class="nav-item">
                    <a class="nav-link" href="homeview">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="menuview">Menu</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="aboutview">About Us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="productview">Products</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/">Log In</a>
                </li>
            </ul>
        </div>
      </div>
    </nav>
  </header>
  @foreach ($users as $user)
      <div class="container">
        <img src="{{ url('images/'.$user->background) }}" alt="Snow" style="width: 1290px; height: 520px;">
            <div class="centered  p-3 mb-2 bg-dark opacity-25" >{{ $user->tagline }}</div>
          </div>   
    @endforeach
<footer class="text-center text-dark fixed-bottom" style="background-color: #fff;">
  <!-- Grid container -->

  <!-- Copyright -->
  <div class="text-center p-1">
    © 2022 Copyright
    <p>Designed and Developed by: Norene Ann B. Rabara</p>
  </div>
  <!-- Copyright -->
</footer>
</form>
</body>

</html>