<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminMenuEditController extends Controller{

      public function index(){
            $users = DB::select('select * from menu');
      return view('adminmenu',['users'=>$users]);
      }

      public function show($id){
            $users = DB::select('select * from menu where id = ?',[$id]);
      return view('adminmenuedit',['users'=>$users]);
      }

      public function edit(Request $request,$id){
            if($request->hasfile('img')){
                  $file = $request->file('img');
                  $extension = $file->getClientOriginalExtension();
                  $filename = time() . '.' . $extension;
                  $file->move('images', $filename);
      
                  DB::update('update menu set image = ? where id = ?',[$filename, $id]);
            }
            
            $users = DB::select('select * from menu');
            return view('adminmenu',['users'=>$users]);
      }
      
      public function view(){
            $users = DB::select('select * from menu');
      return view('menuview',['users'=>$users]);
      }
}
