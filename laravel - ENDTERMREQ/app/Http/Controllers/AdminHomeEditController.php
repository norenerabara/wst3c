<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminHomeEditController extends Controller{

      public function index(){
            $users = DB::select('select * from home');
      return view('adminhome',['users'=>$users]);
      }

      

      public function show($id){
            $users = DB::select('select * from home where id = ?',[$id]);
      return view('adminhomeedit',['users'=>$users]);
      }

      public function edit(Request $request,$id){
            $tagline = $request->input('tagline');
            if($request->hasfile('background')){
                  $file = $request->file('background');
                  $extension = $file->getClientOriginalExtension();
                  $filename = time() . '.' . $extension;
                  $file->move('images', $filename);
      
                  DB::update('update home set background = ?, tagline = ? where id = ?',[$filename, $tagline, $id]);
            }
            
            $users = DB::select('select * from home');
            return view('adminhome',['users'=>$users]);
      }
      public function view(){
            $users = DB::select('select * from home');
      return view('homeview',['users'=>$users]);
      }

}
