<?php
require_once 'conn.php';

unset($_SESSION['facebook_access_token']);
unset($_SESSION['fb_user_id']);
unset($_SESSION['fb_user_name']);

header("Location:login.php");
?>