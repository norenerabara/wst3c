<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    use HasFactory;

    public $primary_key = 'id';
    public $timestamps = true;
    protected $fillable = [
        'training_id',
        'certificate_id',
        'name',
        'description',
        'date',
        'organizer',
        'position',
        'image',
        'template',
    ];
}
