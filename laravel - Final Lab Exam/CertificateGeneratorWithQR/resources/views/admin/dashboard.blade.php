@if (!isset(Auth::user()->username))
    <script>window.location = "/"</script>
@endif
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ url('../css/style.css') }}">
    <link href='https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css' rel='stylesheet'>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script src="https://kit.fontawesome.com/6d6b82be0b.js" crossorigin="anonymous"></script>
    <script type='text/javascript' src=''></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <title>Admin - Dashboard</title>
    
</head>
<body>
<div class="justify-content-between align-items-center shadow nav">
    <div>
        <h2 style="margin: 0">Certificate Generator</h2>
    </div>

    <div class="d-flex flex-row">
        <div class="p-2">
            <form action="/admin/searchCert" method="post" enctype="multipart/form-data"> 
            @csrf      
            <input type="text" class="form-control" name="code" id="colFormLabel" value = "{{ old ('code') }}" placeholder="Search By Certificate ID">
        </div>
        <div class="p-2">
            <button type="submit" class="btn btn-primary" name="save" >Search</button>
        </div>
        </form>

        <div class="p-2">
            @if(isset(Auth::user()->username))
            <a class="nav-link">Welcome, {{ Auth::user()->username }}<span class="sr-only">(current)</span></a>
            @endif
            <a class="nav-link" href="/admin/logout" style="color: white;">Logout</a>
        </div>
    </div>

    


</div>
        <div class="add">
            <form action="/addtraining" method="post" enctype="multipart/form-data" id = "addform">
                <div class="row">
                    @csrf 
                    <div class="col-sm-3">
                        <input type="text" class="form-control input" name="training" id="colFormLabel" placeholder="Seminar/ Training title">
                    </div>

                    <div class="col-sm-2">
                        <input type="date" class="form-control input" name="date" id="colFormLabel" placeholder="Date">
                    </div>

                    <div class="col-sm-4">
                        <button type="reset" class="btn btn-secondary" name="reset" id="reset"><i class='bx by bx-reset'></i>Reset</button>
                        <button type="submit" class="btn btn-primary" name="save" ><i class='bx by bx-plus'></i>Add </button>
                    </div>
                </div>
            </form>
        </div>

        <div class="entry mt-5">
            <h5 style="margin-bottom: 1.5rem;">Seminars/Trainings</h5>
                <div class="row">
                    <div class="col-sm-6">
                        @foreach ($list as $list)
                            <a class="entry-list" href="/view/{{$list->training_id}}">
                                <div class="card w-100 mb-3" style="width: 18rem;">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$list -> training}}</h5>
                                        <p>{{$list -> date}}</p>

                                        <div class="d-flex justify-content-end mt-3">
                                            <a href="/admin/edit/{{$list->training_id}}" class="btn btn-warning"><i class='bx by bxs-edit-alt' ></i>EDIT</a>
                                            <a href="/admin/delete/{{$list->training_id}}" class="btn btn-danger"><i class='bx by bxs-trash-alt' ></i>DELETE</a>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        @endforeach
                    </div>
                </div>
        </div>

<footer class="page-footer font-small justify-content-between align-items-center shadow nav sticky-bottom mt-5">

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">©{{ Date("Y") }} Copyright: PSU
    <a href="/" class="text-white"> </a>
  </div>
  <!-- Copyright -->

</footer>
</body>
</html>