@if (!isset(Auth::user()->username))
    <script>window.location = "/"</script>
@endif
<?php
    $i = str_replace('["',"",$id);
    $d = str_replace('"]',"",$i);
?>
<!doctype html>
<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>Admin Dashboard - Generate</title>
    <link rel="stylesheet" href="{{ url('../css/style.css') }}">
    <link href='https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css' rel='stylesheet'>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script src="https://kit.fontawesome.com/6d6b82be0b.js" crossorigin="anonymous"></script>
    <script type='text/javascript' src=''></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.5/datatables.min.css" />
    <style>@import url("https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap");
    <style>
        body {
            overflow-x:hidden; /* Hide scrollbars */
        }
    </style>
</head>
<body>
<div class="justify-content-between align-items-center shadow nav">
    <div>
        <h2 style="margin: 0">Certificate Generator</h2>
    </div>

    <div class="d-flex">
        <ul class="navbar-nav">
            <li class="nav-item ">
                @if(isset(Auth::user()->username))
                <a class="nav-link">Welcome, {{ Auth::user()->username }}<span class="sr-only">(current)</span></a>
                @endif
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/logout" style="color: white;">Logout</a>
            </li>
        </ul>
    </div>
</div>
    <div class="add">
        <h3 class="mb-3"><a href="{{ url('admin/dashboard') }}"><i class='bx bx-left-arrow-alt'></i></a>{{$d}}</h3>

        <form action="{{ route('generate', $d) }}" method="post" enctype="multipart/form-data"> 
            @csrf
            <input type="hidden" value = "{{ $train }}" name = "training_id">
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        @if (Session::has('success'))
                            <div class="alert alert-success">
                                <li>{{ Session::get('success') }}</li>
                            </div>    
                        @endif
                    </div>
                </div>
                <div class="col-sm-4">
                    <label class="form-label">Name</label>
                    <input type="text" class="form-control" name="name" id="colFormLabel" placeholder="Name">
                    @error('name')
                        <div class="text text-danger mt-2">{{ $message }}</div>
                    @enderror
                </div>
                <div class="col-sm-4">
                    <label class="form-label">Description:</label>
                    <textarea class="form-control" name="description" id="colFormLabel" rows="1" placeholder="Description"></textarea>
                    @error('description')
                        <div class="text text-danger mt-2">{{ $message }}</div>
                    @enderror
                </div>
                <div class="col-sm-4">
                    <label class="form-label">Date</label>
                    <input type="date" class="form-control" name="date" id="colFormLabel">
                    @error('date')
                        <div class="text text-danger mt-2">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            
            <div class="row mt-2">
                <div class="col-sm-4">
                    <label class="form-label">Organizer</label>
                    <input type="text" class="form-control" name="organizer" id="colFormLabel" placeholder="Speaker">
                    @error('organizer')
                        <div class="text text-danger mt-2">{{ $message }}</div>
                    @enderror
                </div>
                <div class="col-sm-4">
                    <label class="form-label">Title/Position</label>
                    <input type="text" class="form-control" name="position" id="colFormLabel" placeholder="Position">
                    @error('position')
                        <div class="text text-danger mt-2">{{ $message }}</div>
                    @enderror
                </div>
                <div class="col-sm-4">
                    <label class="form-label">Logo</label>
                    <input type="file" class="form-control" name="image" id="fileToUpload">
                    @error('image')
                        <div class="text text-danger mt-2">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-sm-4">
                    <label class="form-label">Template</label>
                    <select class="form-control mt-1" style="width:50%" id="background" name = "template">
                        <option value="Template 1" > Template 1</option>
                        <option value="Template 2" > Template 2</option>
                        <option value="Template 3" > Template 3</option>  
                    </select>
                    @error('template')
                        <div class="text text-danger mt-2">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="row mt-2">

            </div>
            <div class="row mt-2">
                <div class="col-sm-4">
                    <button type="submit" class="btn btn-primary" name="save" >Generate </button>
                </div>
            </div>
        </form>
    </div>

    <div class="row mb-3"> 
        <div class="row">
                <div class="col-sm-3">
                </div>
                <div class="col-sm-1">
                    <img src="{{ url('assets/image/template_image/img2.png') }}" alt=" " width="100"> 
                </div>
                <div class="col-sm-1">
                    <img src="{{ url('assets/image/template_image/img3.png') }}" alt=" " width="100"> 
                </div>
                <div class="col-sm-1">
                    <img src="{{ url('assets/image/template_image/img5.jpg') }}" alt=" " width="100"> 
                </div>
                <div class="col-sm-3">
                
                </div>
        </div>     
    </div>

    <!-- {{$qr = QrCode::size(300)->generate('--------');}} -->

    <table id="tblUser">
            <thead>
                <th>QR</th>
                <th>Name</th>
                <th>Description</th>
                <th>Date</th>
                <th>Organizer/Speaker</th>
                <th>Position/Title</th>
            </thead>
            <tbody>
    
          


        @if ($fetch != NULL)
        @foreach ($fetch as $item)
                        <tr>
                            <td> <a href = "/admin/view/certificate/{{ $item->id }} " target="_blank">{{$qr = QrCode::size(50)->generate('item->certificate_id');}}</a> </td>
                            <td> {{$item->name}}</td>
                            <td> {{$item->description}}</td>
                            <td> {{$item->date}}</td>
                            <td> {{$item->organizer}}</td>
                            <td> {{$item->position}}</td>
                            
                        </tr>
                
             @endforeach
        @endif
            </tbody>
        </table>


    

<script type='text/javascript' src='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js'></script>

<footer class="page-footer font-small justify-content-between align-items-center shadow nav sticky-bottom" style="margin-top: 84px;">

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">©{{ Date("Y") }} Copyright: PSU
    <a href="/" class="text-white"> </a>
  </div>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.5/datatables.min.js"></script>
<script>
jQuery(document).ready(function($) {
    $('#tblUser').DataTable();
} );
</script>

</body>

</html>
