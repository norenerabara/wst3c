<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ApptController;
use App\Http\Controllers\RegController;
use App\Http\Controllers\LogInEtrController;
use App\Http\Controllers\ApproveController;
use App\Http\Controllers\StudentInsertController;
use App\Http\Controllers\StudViewController;
use App\Http\Controllers\AdminMenuController;
use App\Http\Controllers\AdminMenuEditController;
use App\Http\Controllers\AdminHomeEditController;
use App\Http\Controllers\AboutInsertController;
use App\Http\Controllers\AboutEditController;
use App\Http\Controllers\AboutDeleteController;
use App\Http\Controllers\ProductEditController;
use App\Http\Controllers\LogInFinalsController;
use App\Http\Controllers\RegFinalsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::get('/insert',[ApptController::class, 'insertform']);
//Route::post('/create',[ApptController::class, 'insert']);

//Route::get('/reginsert',[RegController::class, 'insertreg']);
//Route::post('/createe',[RegController::class, 'reginsert']);

//Route::get('/loginsert',[LogController::class, 'insertlog']);
//Route::post('/createee',[LogController::class, 'loginsert']);

//Route::get('/view-records',[ApproveController::class, 'viewAppointment']);

//Route::get('/', [OrderController::class, 'index']);

// Route::get('/', [LogInEtrController::class, 'index']);
// Route::get('/admin', [LogInEtrController::class, 'successLogin']);
// Route::post('/validate', [LogInEtrController::class, 'validateForm'])->name('login.check');
// Route::get('/logout', [LogInEtrController::class, 'logout']);

//Route::get('/adminmenu', [AdminMenuController::class, 'index']);

// Route::get('/adminmenu', [AdminMenuEditController::class, 'index']);
// Route::get('edit/{id}', [AdminMenuEditController::class, 'show']);
// Route::post('edit/{id}', [AdminMenuEditController::class, 'edit']);

// Route::get('/adminhome', [AdminHomeEditController::class, 'index']);
// Route::get('hedit/{id}', [AdminHomeEditController::class, 'show']);
// Route::post('hedit/{id}', [AdminHomeEditController::class, 'edit']);

// Route::get('/aboutinsert', [AboutInsertController::class, 'insertform']);
// Route::post('/create', [AboutInsertController::class, 'insert']);

// Route::get('/about', [AboutEditController::class, 'index']);
// Route::get('aedit/{id}', [AboutEditController::class, 'show']);
// Route::post('aedit/{id}', [AboutEditController::class, 'edit']);
// Route::get('delete/{id}', [AboutEditController::class, 'destroy']);

// Route::get('/homeview', [AdminHomeEditController::class, 'view']);

// Route::get('/menuview', [AdminMenuEditController::class, 'view']);

// Route::get('/aboutview', [AboutEditController::class, 'view']);

// Route::get('/products', [ProductEditController::class, 'index']);
// Route::get('pedit/{id}', [ProductEditController::class, 'show']);
// Route::post('pedit/{id}', [ProductEditController::class, 'edit']);


// Route::get('/productview', [ProductEditController::class, 'views']);

Route::get('/', [LogInFinalsController::class, 'index']);
Route::get('/dashboard', [LogInFinalsController::class, 'successLogin']);
Route::post('/validate', [LogInFinalsController::class, 'validateForm'])->name('login.check');
Route::get('/logout', [LogInFinalsController::class, 'logout']);

Route::get('/regfinals', [RegFinalsController::class, 'index']);
Route::post('/regfinals/create', [RegFinalsController::class, 'validateForm'])->name('create.account');
//Route::get('/customer/{customerID}/{name}/{address}', [OrderController::class, 'customer'])->name('customer');

//Route::get('/item/{itemNo}/{name}/{price}', [OrderController::class, 'item'])->name('item');

//Route::get('/order/{customerID}/{name}/{orderNo}/{date}', [OrderController::class, 'order'])->name('order');

//Route::get('/orderDetails/{transNo}/{orderNo}/{itemID}/{name}/{price}/{qty}', [OrderController::class, 'orderDetails'])->name('orderDetails');

//Route::redirect('/here', '/there', 404);


