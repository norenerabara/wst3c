<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AboutDeleteController extends Controller{
public function index(){
      $users = DB::select('select * from about');
return view('about',['users'=>$users]);
}
public function destroy($id){
      DB::delete('delete from about where id = ?',[$id]);
}
}
