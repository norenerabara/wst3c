<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AboutEditController extends Controller{

      public function index(){
            $users = DB::select('select * from about');
      return view('about',['users'=>$users]);
      }

      public function show($id){
            $users = DB::select('select * from about where id = ?',[$id]);
      return view('aboutedit',['users'=>$users]);
      }

      public function edit(Request $request,$id){
            $name = $request->input('name');
            $position = $request->input('position');
            $photo = $request->input('photo');
                  DB::update('update about set name = ?, position = ?, photo = ? where id = ?',[$name, $position, $photo, $id]);
            
            $users = DB::select('select * from about');
            return view('about',['users'=>$users]);
      }
      public function destroy($id){
            $res = DB::delete('delete from about where id = ?',[$id]);
            if($res){
                return back()->with('success', 'Succesfully deleted!');
            } else{
                return back()->with('fail', 'Something went wrong');
            }
      }

      public function view(){
            $users = DB::select('select * from about');
      return view('aboutview',['users'=>$users]);
      }
      
}
