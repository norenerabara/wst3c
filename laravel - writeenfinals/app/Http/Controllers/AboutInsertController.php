<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AboutInsertController extends Controller{
public function insertform(){
return view('aboutinsert');
}
	
public function insert(Request $request){
      $name = $request->input('name');
      $position = $request->input('position');
      $photo = $request->input('photo');
      DB::insert('insert into about (name, position, photo) values(?,?,?)',[$name, $position, $photo]);
      return view('aboutinsert');
}
}

