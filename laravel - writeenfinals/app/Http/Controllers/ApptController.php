<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class ApptController extends Controller
{
    public function insertform(){
        return view('appointment');
    }
    
    public function insert(Request $request){
        $fullname = $request->input('fullname');
        $time = $request->input('time');
        $date = $request->input('date');
        $reason = $request->input('reason');
        $appoint = DB::select('select * from appointment');

            if($appoint=4){
                DB::insert('insert into appointment (fullname, time, date, reason) values(?,?,?,?)', [$fullname, $time, $date, $reason]);
                echo "<div class='alert alert-success'> Approved</div>";
                return view('aptapprove');
            }
            else{
                echo "<div class = 'alert alert-danger'> Appointment Slot Full!</div>";
                return view('appointment');
            }
    }
}
