<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegFinalsController extends Controller
{
    public function index()
    {
        return view('regfinals');
    }

    public function validateForm(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'eMail' => ['required', 'unique:users', 'email'],
            'username' => ['required', 'min:6'],
            'password' => ['required', 'min:6'],
            'confirmpassword' => ['required', 'min:6'],
            
        ]);
        
        if($validator->fails()){
            return redirect('/regfinals')->withErrors($validator)->withInput();
        }
        else{
            $email = $request->get('eMail');
            $username = $request->get('username');
            $password = Hash::make($request->get('password'));
            $confirmpassword = Hash::make($request->get('confirmpassword'));
            

            DB::insert('insert into users(Email, username, password, confirmpassword)
            values(?,?,?,?)', [$email, $username, $password, $confirmpassword]);

            return redirect('/regfinals')->with('success', 'Registered Successfully!');
        }    
    }
}
