<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductEditController extends Controller{

      public function index(){
            $users = DB::select('select * from products');
      return view('products',['users'=>$users]);
      }

      public function show($id){
            $users = DB::select('select * from about where id = ?',[$id]);
      return view('productsedit',['users'=>$users]);
      }

      public function edit(Request $request,$id){
            $prodphoto = $request->input('prodphoto');
            $prodname = $request->input('prodname');
                DB::update('update products set prodphoto = ?, prodname = ? where id = ?',[$prodphoto, $prodname, $id]);
            
            $users = DB::select('select * from products');
            return view('products',['users'=>$users]);
      }

      public function views(){
            $users = DB::select('select * from products');
      return view('productview',['users'=>$users]);
      }
      
}
