<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class LogInEtrController extends Controller
{
    public function index()
    {
        return view('loginetr');
    }

    public function validateForm(Request $request)
    {
        $this->validate($request, [
            'username'=>'required',
            'password'=>'required|alphaNum|min:6',
        ]);

        $user_data = array(
            'username' =>  $request->get('username'),
            'password'  =>  $request->get('password'),
        );

        $username = $request->input('username');
        $password = $request->input('password');

        if($username == 'admin' && $password == 'admin123'){
            return redirect('/adminhome');
        }
        else{
            return back()->with('error', 'Wrong Login Details!');
        }
        
    }

    public function successLogin()
    {
        return view('adminhome');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
