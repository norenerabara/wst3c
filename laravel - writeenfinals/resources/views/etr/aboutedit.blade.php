<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&family=Spline+Sans+Mono&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&family=Acme&family=Courgette&family=Kdam+Thmor+Pro&family=Pacifico&family=Spline+Sans+Mono&display=swap" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <title>About Us</title>

    <style>
        .body{
            background-color: #000;
        }
        .h5{
            color: #fff;
        }
        .nav-link{
            color: #fff;
        }
        .title{
            color: #fff;
            font-style: bold;
            font-family: 'Spline Sans Mono', monospace;
            font-size: 30px;
            margin-left: 30px;
        }
        .nav-link:hover{
          color: #000;
        }
        .nav a{
          font-family: 'Kdam Thmor Pro', sans-serif;
          color: #fff;
        }
    </style>
    
</head>
<body class="body">
<header class="header">
    <nav class="navbar navbar-expand-lg bg-danger">
      <div class="container-fluid">
          <div class="title"> 
          Cafemoza
    </div>
            <ul class="nav justify-content-end">
                <li class="nav-item">
                    <a class="nav-link" href="adminhome">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="adminmenu">Menu</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="about">About Us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="products">Products</a>
                </li>
            </ul>
        </div>
      </div>
    </nav>
  </header>
  <form action = "/aedit/<?php echo $users[0]->id; ?>" method = "post" enctype="multipart/form-data">
                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                    <div class="container" style="margin-top: 90px;">
                    <div class="card">
                    <div class="card-body" style="margin-left: 160px;">    
                    <div class="col-md-10">
                            <label for="formFile" class="form-label">Name</label>
                            <input class="form-control" type="text" id="formFile" name="name" value = '<?php echo $users[0]->name; ?>' required>
                            <br>
                            <label for="formFile" class="form-label">Position</label>
                            <input class="form-control" type="text" id="formFile" name="position" value = '<?php echo $users[0]->position; ?>' required>
                            <br>
                            <label for="formFile" class="form-label">Photo</label>
                            <input class="form-control" type="text" id="formFile" name="photo" value = '<?php echo $users[0]->photo; ?>' required>
                        </div>
                        <div class="d-grid gap-2 col-6" style="margin-left: 140px;">
                            <button type="submit" class="btn btn-dark mt-3 mb-3">Update Content</button>
                        </div>
    </div>
    </div>
    </div>
                </form>
<footer class="text-center text-dark fixed-bottom" style="background-color: #fff;">
  <!-- Grid container -->

  <!-- Copyright -->
  <div class="text-center p-1">
    © 2022 Copyright
    <p>Designed and Developed by: Norene Ann B. Rabara</p>
  </div>
  <!-- Copyright -->
</footer>
</body>

</html>