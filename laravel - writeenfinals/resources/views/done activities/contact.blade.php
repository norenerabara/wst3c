<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel - Contact Us</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

        <style>
            body{
                background-image: url('images/contb.jpg');
                height: 280vh;
            }
        </style>
</head>
<body>
<nav class="navbar navbar-expand-lg bg-light navbar-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">
      <img src="images/logoo.jpg" style="width:60px; height: 40px; margin-left: 100px;" class="rounded-pill"> 
    </a>
<div class="collapse navbar-collapse justify-content-between">
<ul class="nav justify-content-end" style="margin-left:600px;">
  <li class="nav-item">
    <a class="nav-link active" aria-current="page" href="home">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="about">About</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="contact">Contact Us</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="login">Log In</a>
    <li class="nav-item">
    <a class="nav-link" href="register">Registration</a>
  </li>
  </li>
</ul>
        </div>
        </nav>
  <div class="container">
  <p style="font-family:courier; font-size:280%; text-align:center; color:white; margin-top:5px;">My Gallery</p>
  <img src="images/me2.jpg" class="float-start" style="width: 250px; height: 400px; margin-top:50px;">
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <img src="images/me4.jpg" style="width: 250px; height: 400px; margin-top:50px;">
  <img src="images/me3.jpg" class="float-end" style="width: 250px; height: 400px; margin-top:50px;">
  <br><br><br><br>
  <img src="images/me5.jpg" class="float-start" style="width: 250px; height: 400px; margin-top:50px;">
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <img src="images/me6.jpg" style="width: 250px; height: 400px; margin-top:50px;">
  <img src="images/me7.jpg" class="float-end" style="width: 250px; height: 400px; margin-top:50px;">
          </div>
          <br><br><br><br><br><br><br>
          <div class="container p-3 my-2 mt-5 border" style="width: 500px; height: 300px;">
          <p style="font-family:courier; font-size:250%; text-align:center; color:white;"><b>Contact Us</b></p>
          <p style="font-family:courier; font-size:150%; text-align:center; color:white; margin-top:5px;"><img src="images/fb.png" style="width: 50px; height: 50px;">&nbsp;&nbsp;FB: Ann Berbon</p>
          <p style="font-family:courier; font-size:150%; text-align:center; color:white; margin-top:5px;"><img src="images/igg.png" style="width: 60px; height: 45px;">IG: its.riiiinnn</p>
          <p style="font-family:courier; font-size:150%; text-align:center; color:white; margin-top:5px;"><img src="images/phone.png" style="width: 40px; height: 35px;">&nbsp;Phone Number: 09156729103</p>
          </div>
                        
</body>
</html>