<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
    <title>Homepage</title>
    <style>
        a{
            text-decoration: none;
            color: black;
            font-weight: bold;
        }

        a:hover{
            color:white;
        }
    </style>
</head>
<body>
  <nav class="navbar navbar-expand-lg bg-danger navbar-danger">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">
      <img src="images/logo.jpg" style="width:55px; height: 53px; margin-left: 100px;" class="rounded-pill"> 
    </a>
<div class="collapse navbar-collapse justify-content-between">
<ul class="nav justify-content-end" style="margin-left:600px;">
  <li class="nav-item">
    <a class="nav-link-dark" aria-current="page" href="home">Home</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </li>
  <li class="nav-item">
    <a class="nav-link-dark" href="menu">Menu</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </li>
  <li class="nav-item">
    <a class="nav-link-dark" href="about">About</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </li>
  <li class="nav-item">
    <a class="nav-link-dark" href="login">Log In</a>
  </li>
</ul>
        </div>
        </nav>             
</body>
</html>