<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <title>Appointment</title>
</head>
<body>
<form action="/create"method="post">
<input type = "hidden" name = "_token" value = "<?php echo csrf_token();?>">
<div class="container" style="margin-top: 90px; margin-left: 250px;">
<div class="card mb-3" style="max-width: 840px; max-height: 600px; padding: 10px;">
  <div class="row g-0">
    <div class="col-md-4">
      <img src="images/online-appointment.png" class="img-fluid rounded-start" style="margin-top: 70px;">
    </div>
    <div class="col-md-8">
      <div class="card-body">
        <h5 class="card-title">Fill-Up!</h5>
        <br>
        <div class="input-group mb-3">
            <span class="input-group-text" id="basic-addon1">Fullname</span>
            <input type="text" class="form-control" name="fullname" placeholder="Fullname" aria-label="Fullname" aria-describedby="basic-addon1">
        </div>
        <div class="input-group mb-3">
            <span class="input-group-text" id="basic-addon1">Time</span>
            <input type="text" class="form-control" name="time" placeholder="Time" aria-label="Pet's Age" aria-describedby="basic-addon1">
        </div>
        <div class="input-group mb-3">
            <span class="input-group-text" id="basic-addon1">Date</span>
            <input type="date" class="form-control" name="date" placeholder="date" aria-label="Pet's Age" aria-describedby="basic-addon1">
        </div>
        <div class="input-group">
          <span class="input-group-text">Reason</span>
          <textarea class="form-control" name="reason" aria-label="With textarea"></textarea>
        </div><br>
        <input type="submit" value="Set" class="btn btn-dark">
      </div>
    </div>
  </div>
</div>
</div>
</form>
</body>
</html>