<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ApptController;
use App\Http\Controllers\RegController;
use App\Http\Controllers\LogController;
use App\Http\Controllers\ApproveController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/activity1', function () {
    return view('activity1');
});

Route::get('/login', function () {
    return view('login');
});

Route::get('/homepage', function () {
    return view('homepage');
});

Route::get('/homep', function () {
    return view('homep');
});

Route::get('/register', function () {
    return view('register');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/student', function () {
    return view('student');
});

Route::get('/loginAC', function () {
    return view('loginAC');
});

Route::get('/aptlogin', function () {
    return view('aptlogin');
});

Route::get('/aptregister', function () {
    return view('aptregister');
});

Route::get('/appointment', function () {
    return view('appointment');
});

Route::get('student/details', function () {
    $url="Name: Norene Ann B. Rabara" . "<br>" . "Section: 3C";
    return $url;
})->name('student.details');

Route::get('/insert',[ApptController::class, 'insertform']);
Route::post('/create',[ApptController::class, 'insert']);

Route::get('/reginsert',[RegController::class, 'insertreg']);
Route::post('/createe',[RegController::class, 'reginsert']);

Route::get('/loginsert',[LogController::class, 'insertlog']);
Route::post('/createee',[LogController::class, 'loginsert']);

Route::get('/view-records',[ApproveController::class, 'viewAppointment']);

Route::get('/', [OrderController::class, 'index']);

Route::get('/customer/{customerID}/{name}/{address}', [OrderController::class, 'customer'])->name('customer');

Route::get('/item/{itemNo}/{name}/{price}', [OrderController::class, 'item'])->name('item');

Route::get('/order/{customerID}/{name}/{orderNo}/{date}', [OrderController::class, 'order'])->name('order');

Route::get('/orderDetails/{transNo}/{orderNo}/{itemID}/{name}/{price}/{qty}', [OrderController::class, 'orderDetails'])->name('orderDetails');

Route::redirect('/here', '/there', 404);


