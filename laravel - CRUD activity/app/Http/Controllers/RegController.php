<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class RegController extends Controller
{
    public function insertreg(){
        return view('aptregister');
    }
    
    public function reginsert(Request $request){
      $fullname = $request->input('fullname');
      $email = $request->input('email');
      $age = $request->input('age');
      $contact = $request->input('contact');
      $register = DB::select('select * from register');
        DB::insert('insert into register (fullname, email, age, contact) values(?,?,?,?)', [$fullname, $email, $age, $contact]);
        echo "<div class='alert alert-success'> Registered</div>";
            return view('aptregister');
            
    }
}
