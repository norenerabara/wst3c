<?php

define('APP_ID', '906949466638174');
define('APP_SECRET', 'e31d60dcb9ff75836ed9b568a164705c');
define('API_VERSION', 'v2.5');
define('FB_BASE_URL','htt://localhost/loginfb/');

if(!session_id()){
    session_start();
}

require_once(__DIR__.'/Facebook/autoload.php');

$fb = new Facebook\Facebook([
    'app_id' => APP_ID,
    'app_secret' => APP_SECRET,
    'default_graph_version' => API_VERSION,
]);

$fb_helper = $fb->getRedirectLoginHelper();

try{
    if(isset($_SESSION['facebook_access_token']))
        {$accessToken = $_SESSION['facebook_access_token'];}
    else
        {$accessToken = $fb_helper->getAccessToken();}
}

catch(FacebookResponseException $e){
    echo 'Facebook API Error' . $e->getMessage();
    exit;
}

catch(FacebookSDKException $e){
    echo 'Facebook SDK Error' . $e->getMessage();
    exit;
}

?>
